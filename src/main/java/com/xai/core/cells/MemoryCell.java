package com.xai.core.cells;

import com.google.inject.Inject;
import com.xai.infrastructure.logging.Logger;

import java.util.UUID;

public class MemoryCell extends Cell {

    @Inject
    public MemoryCell(Logger logger) {
        super(logger);
    }

    @Override
    protected void runImpl() {

    }
}
