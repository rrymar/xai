package com.xai.core.cells;

import com.google.inject.Inject;
import com.xai.infrastructure.logging.Logger;

import java.util.UUID;

public class ManagerCell extends Cell {

    @Inject
    public ManagerCell(Logger logger) {
        super(logger);
    }

    @Override
    protected void runImpl() {
        logger.log("Manager cell started %s", this.getId());
    }
}
