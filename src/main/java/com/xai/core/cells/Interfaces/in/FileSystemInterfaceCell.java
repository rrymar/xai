package com.xai.core.cells.Interfaces.in;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.xai.core.cells.Cell;
import com.xai.infrastructure.logging.Logger;

import java.io.IOException;
import java.nio.file.*;
import java.util.List;

@Singleton
public class FileSystemInterfaceCell extends Cell implements InInterfaceCell {

    public final static String Name = "FileSystemInterfaceCell";

    private WatchService watcher;

    @Inject
    public FileSystemInterfaceCell(Logger logger) {
        super(logger);
    }

    @Override
    protected void runImpl() {
        Path dir = Paths.get("c:/temp/xaiIn");
        try {
            watcher = dir.getFileSystem().newWatchService();
            dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
            while (true) {
                Thread.sleep(1000L);
            }
        /*    WatchKey watchKey = watcher.take();

            while (watchKey != null) {

                List<WatchEvent<?>> events = watchKey.pollEvents();
                for (WatchEvent event : events) {
                    logger.log("%s %s", event.kind(), event.context());
                }

                watchKey.reset();
                watchKey = watcher.take();
            }        */
        } catch (IOException e) {
            logger.log(e);
        } catch (InterruptedException e) {
            logger.log(e);
        }
    }

    @Override
    public String get() {
        try {
            String message = "";
            WatchKey watchKey = watcher.take();

            List<WatchEvent<?>> events = watchKey.pollEvents();
            for (WatchEvent event : events) {
                message = String.format("%s %s %s", message, event.kind(), event.context());
            }

            watchKey.reset();

            return message;

        } catch (InterruptedException e) {
            logger.log(e);
            return "error";
        }
    }
}
