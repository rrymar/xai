package com.xai.infrastructure.running;

import com.xai.core.cells.Cell;

public class ThreadCellRunner implements CellRunner {
    @Override
    public void run(Cell cell) {
        (new Thread(cell)).start();
    }

    @Override
    public void runAsDemon(Cell cell) {
        Thread thread = new Thread(cell);
        thread.setDaemon(true);
        thread.start();
    }
}
