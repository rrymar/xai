package com.xai.core.cells.collections;

import com.xai.core.cells.Cell;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class SimpleCellsCollection implements CellsCollection {
    public static final String Name = "Simple";

    private List<Cell> cells;

    public SimpleCellsCollection() {
        this.cells = new ArrayList<Cell>();
    }

    @Override
    public int size() {
        return cells.size();
    }

    @Override
    public boolean isEmpty() {
        return cells.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return cells.contains(o);
    }

    @Override
    public Iterator<Cell> iterator() {
        return cells.iterator();
    }

    @Override
    public Object[] toArray() {
        return cells.toArray();
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return cells.toArray(a);
    }

    @Override
    public boolean add(Cell cell) {
        return cells.add(cell);
    }

    @Override
    public boolean remove(Object o) {
        return cells.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return cells.containsAll(c);
    }

    @Override
    public boolean addAll(Collection<? extends Cell> c) {
        return cells.addAll(c);
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return cells.removeAll(c);
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return cells.retainAll(c);
    }

    @Override
    public void clear() {
        cells.clear();
    }
}
