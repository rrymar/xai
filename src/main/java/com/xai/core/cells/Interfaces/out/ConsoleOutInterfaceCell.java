package com.xai.core.cells.Interfaces.out;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.xai.core.cells.Cell;
import com.xai.infrastructure.logging.Logger;

@Singleton
public class ConsoleOutInterfaceCell extends Cell implements OutInterfaceCell  {

    public final static String Name =  "ConsoleOutInterfaceCell";

    @Inject
    public ConsoleOutInterfaceCell(Logger logger) {
        super(logger);
    }

    @Override
    protected void runImpl() {
        while(true){
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                logger.log(e);
                break;
            }
        }
    }

    public void push(String message){
        System.out.println(message);
        System.out.flush();
    }
}
