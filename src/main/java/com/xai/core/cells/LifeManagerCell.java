package com.xai.core.cells;

import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.name.Named;
import com.xai.core.cells.Interfaces.InterfaceCell;
import com.xai.core.cells.collections.CellsCollection;
import com.xai.core.cells.collections.SimpleCellsCollection;
import com.xai.infrastructure.logging.Logger;
import com.xai.infrastructure.running.CellRunner;
import com.xai.runner.DI;

public class LifeManagerCell extends Cell {
    @Inject
    @Named(SimpleCellsCollection.Name)
    private CellsCollection managerCells;

    @Inject
    @Named(SimpleCellsCollection.Name)
    private CellsCollection interfaceCells;

    private final CellRunner cellRunner;

    @Inject
    public LifeManagerCell(Logger logger, CellRunner cellRunner ) {
        super(logger);
        this.cellRunner = cellRunner;
    }

    @Override
    protected void runImpl() {
        runManagerCell(DI.Injector.getInstance(ManagerCell.class));
        runManagerCell(DI.Injector.getInstance(ManagerCell.class));
        runManagerCell(DI.Injector.getInstance(ManagerCell.class));


        logger.log("LifeManagerCell started");

        for (Key<InterfaceCell> item : DI.getAllKeysByParentType(InterfaceCell.class)) {
            runInterfaceCell(DI.Injector.getInstance(item));
            logger.log(item.getTypeLiteral().getType().toString() + " started");
        }

        runManagerCell(DI.Injector.getInstance(ExecutorCell.class));

    }

    private void runInterfaceCell(InterfaceCell cell) {
        interfaceCells.add((Cell)cell);
        cellRunner.run((Cell)cell);
    }

    private void runManagerCell(Cell cell ) {
        managerCells.add(cell);
        cellRunner.runAsDemon(cell);
    }
}
