package com.xai.runner;

import com.xai.core.cells.LifeManagerCell;

public class ConsoleRunner {
    public static void main(String[] args) {
        System.out.println("Starting");
        Thread lifeManager = new Thread(DI.Injector.getInstance(LifeManagerCell.class));
        lifeManager.start();
        System.out.println("Started");
    }
}
