package com.xai.core.cells.Interfaces.out;

import com.xai.core.cells.Interfaces.InterfaceCell;

public interface OutInterfaceCell extends InterfaceCell {
    void push(String message);
}
