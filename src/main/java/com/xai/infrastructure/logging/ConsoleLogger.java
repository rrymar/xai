package com.xai.infrastructure.logging;

public class ConsoleLogger implements Logger {

    @Override
    public void log(String message, Object... args) {
        log(String.format(message, args));
    }

    @Override
    public void log(Throwable t) {
        log(t.toString());
        for (StackTraceElement item : t.getStackTrace()) {
            log(item.toString());

        }
    }

    @Override
    public void log(String message) {
        System.out.println(message);
        System.out.flush();
    }
}
