package com.xai.infrastructure.running;

import com.xai.core.cells.Cell;

public interface CellRunner {
    void run(Cell cell);

    void runAsDemon(Cell cell);
}
