package com.xai.core.cells.Interfaces.in;

import com.xai.core.cells.Interfaces.InterfaceCell;

public interface InInterfaceCell extends InterfaceCell {
    String get();
}
