package com.xai.core.cells;

import com.google.inject.Inject;
import com.xai.infrastructure.logging.Logger;

import java.util.UUID;

public abstract class Cell implements Runnable {
    private final UUID id;

    protected final Logger logger;

    @Inject
    public Cell(Logger logger) {
        this.id = UUID.randomUUID();
        this.logger = logger;
    }

    protected abstract void runImpl();

    @Override
    public void run() {
        try {
            runImpl();
        } catch (Throwable t) {
            logger.log(t);
        }
    }

    public UUID getId() {
        return id;
    }
}
