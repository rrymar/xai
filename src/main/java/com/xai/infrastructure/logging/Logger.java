package com.xai.infrastructure.logging;

public interface Logger {
    void log(String message, Object... args);

    void log(Throwable t);

    void log(String message);
}
