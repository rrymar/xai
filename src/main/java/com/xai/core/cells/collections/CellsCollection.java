package com.xai.core.cells.collections;

import com.xai.core.cells.Cell;

import java.util.Collection;
import java.util.List;

public interface CellsCollection extends Collection<Cell> {
}
