package com.xai.runner;

import com.google.inject.*;
import com.google.inject.name.Names;
import com.xai.core.cells.Interfaces.InterfaceCell;
import com.xai.core.cells.Interfaces.in.FileSystemInterfaceCell;
import com.xai.core.cells.Interfaces.out.ConsoleOutInterfaceCell;
import com.xai.core.cells.collections.CellsCollection;
import com.xai.core.cells.collections.SimpleCellsCollection;
import com.xai.infrastructure.logging.ConsoleLogger;
import com.xai.infrastructure.logging.Logger;
import com.xai.infrastructure.running.CellRunner;
import com.xai.infrastructure.running.ThreadCellRunner;

import java.util.ArrayList;
import java.util.List;

public class DI extends AbstractModule {
    public final static Injector Injector = Guice.createInjector(new DI());

    @Override
    protected void configure() {
        bind(Logger.class).to(ConsoleLogger.class).in(Singleton.class);
        bind(CellRunner.class).to(ThreadCellRunner.class).in(Singleton.class);
        bind(CellsCollection.class).annotatedWith(Names.named(SimpleCellsCollection.Name)).to(SimpleCellsCollection.class).in(Singleton.class);

        bind(InterfaceCell.class).annotatedWith(Names.named(ConsoleOutInterfaceCell.Name)).to(ConsoleOutInterfaceCell.class).in(Singleton.class);
        bind(InterfaceCell.class).annotatedWith(Names.named(FileSystemInterfaceCell.Name)).to(FileSystemInterfaceCell.class).in(Singleton.class);
    }

    public static <T> List<Key<T>> getAllKeysByParentType(Class<T> type) {
        List<Key<T>> toReturn = new ArrayList<Key<T>>();
        for (Key<?> item : Injector.getAllBindings().keySet()) {
            if(item.getTypeLiteral().getRawType().isInterface()) {
                continue;
            }

            if (InterfaceCell.class.isAssignableFrom(item.getTypeLiteral().getRawType())) {
                toReturn.add((Key<T>) item);
            }
        }

        return toReturn;
    }

    public static <T> T getInstanceByName(Class<T> type, String name) {
        return Injector.getInstance(Key.get(type, Names.named(name)));
    }

    public static <T> T getInstance(Class<T> type) {
        return Injector.getInstance(type);
    }
}
