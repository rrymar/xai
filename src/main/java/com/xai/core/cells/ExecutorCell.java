package com.xai.core.cells;

import com.google.inject.Inject;
import com.xai.core.cells.Interfaces.InterfaceCell;
import com.xai.core.cells.Interfaces.in.FileSystemInterfaceCell;
import com.xai.core.cells.Interfaces.in.InInterfaceCell;
import com.xai.core.cells.Interfaces.out.ConsoleOutInterfaceCell;
import com.xai.core.cells.Interfaces.out.OutInterfaceCell;
import com.xai.infrastructure.logging.Logger;
import com.xai.runner.DI;

public class ExecutorCell extends Cell {
    @Inject
    public ExecutorCell(Logger logger) {
        super(logger);
    }

    @Override
    protected void runImpl() {
        InInterfaceCell in = (InInterfaceCell) DI.getInstanceByName(InterfaceCell.class, FileSystemInterfaceCell.Name);
        OutInterfaceCell out = (OutInterfaceCell) DI.getInstanceByName(InterfaceCell.class, ConsoleOutInterfaceCell.Name);

        while(true){
            String message = in.get();
            out.push("Forwarder from in " + message);
        }
    }
}
